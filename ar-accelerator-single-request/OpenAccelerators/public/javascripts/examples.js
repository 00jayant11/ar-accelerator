export const examples = [
  {
    "name": "Contextualize Alt Text",
    "input": `
<html>

<head>
  <title>Tom the cat</title>
</head>

<body>
  <h2>Tom</h2>
  <p>Tom is a cat. He lives in a townhouse at Penssylvania. Monday morning Tom is very sad. His owner has moved on to Texas</p>
Tom thinks he doesn't have friends and will be alone
  <figure>
    <img src="cat.jpg" alt="">
    <figcaption>cat at window</figcaption>
  </figure>
Tom immediately see Ben outside the window. Ben a cat walking in the driveway appears to be new friend.
 <figure>
    <img src="cat_meeting.jpg" alt="">
    <figcaption></figcaption>
  </figure>
</body>

</html>
    `,
    "output": `
<html>

<head>
  <title>Tom the cat</title>
</head>

<body>
  <h2>Tom</h2>
  <p>Tom is a cat who lives in a townhouse in Pennsylvania. On Monday morning, Tom is feeling very sad because his owner has moved to Texas. He thinks he doesn't have any friends and will be alone.</p>
  <figure>
    <img src=""cat.jpg"" alt=""Tom the cat looking out the window"">
    <figcaption>Tom looking out the window</figcaption>
  </figure>
  <p>However, Tom immediately sees Ben outside the window. Ben is a cat walking in the driveway and appears to be a new friend.</p>
  <figure>
    <img src=""cat_meeting.jpg"" alt=""Tom and Ben meeting each other"">
    <figcaption>Tom and Ben meeting each other</figcaption>
  </figure>
</body>

</html>

In the first figure, the alt text has been optimized to describe the image of Tom looking out the window. The figcaption has been contextualized to provide more information about the image.

In the second figure, the alt text has been optimized to describe the image of Tom and Ben meeting each other. The figcaption has been contextualized to provide more information about the image.
    `
  },
  {
    "name": "Make Table Accessible",
    "input": `
<table>
    <tr>
        <td><strong>Item</strong></td>
        <td><strong>Threaded screws</strong></td>
        <td><strong>Flat nails</strong></td>
        <td><strong>Dyna-bolts</strong></td>
        <td><strong>Spring washers</strong></td>
    </tr>
    <tr>
        <td><strong>Kilo</strong></td>
        <td>$2.50</td>
        <td>$3.50</td>
        <td>$4.50</td>
        <td>$2.50</td>
    </tr>
    <tr>
        <td><strong>Pound</strong></td>
        <td>$2.00</td>
        <td>$3.00</td>
        <td>$4.00</td>
        <td>$2.00</td>
    </tr>
</table>
    `,
    "output": `
<table>
    <caption>Price list for hardware items</caption>
    <thead>
        <tr>
            <th scope=""col"">Item</th>
            <th scope=""col"">Threaded screws</th>
            <th scope=""col"">Flat nails</th>
            <th scope=""col"">Dyna-bolts</th>
            <th scope=""col"">Spring washers</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope=""row"">Kilo</th>
            <td>$2.50</td>
            <td>$3.50</td>
            <td>$4.50</td>
            <td>$2.50</td>
        </tr>
        <tr>
            <th scope=""row"">Pound</th>
            <td>$2.00</td>
            <td>$3.00</td>
            <td>$4.00</td>
            <td>$2.00</td>
        </tr>
    </tbody>
</table>

Description: This is a price list for hardware items. The table has a caption that describes its purpose. The table has a header row that lists the names of the items and four columns that list the prices for each item in kilos and pounds. The first column is a row header that lists the unit of measurement for the prices. The table is properly structured with the use of the <thead>, <tbody>, <th>, and <td> elements.
    `
  },
  {
    "name": "Generate Keywords",
    "input": `
<p>In physics, a force is an influence that can change the motion of an object. A force can cause an object with mass to change its velocity (e.g. moving from a state of rest), i.e., to accelerate. Force can also be described intuitively as a push or a pull. A force has both magnitude and direction, making it a vector quantity. It is measured in the SI unit of newton (N). Force is represented by the symbol F (formerly P).</p>
<p>&nbsp;</p>
<p>The original form of Newton's second law states that the net force acting upon an object is equal to the rate at which its momentum changes with time. If the mass of the object is constant, this law implies that the acceleration of an object is directly proportional to the net force acting on the object, is in the direction of the net force, and is inversely proportional to the mass of the object.</p>
<p>&nbsp;</p>
<p>Concepts related to force include: thrust, which increases the velocity of an object; drag, which decreases the velocity of an object; and torque, which produces changes in rotational speed of an object. In an extended body, each part usually applies forces on the adjacent parts; the distribution of such forces through the body is the internal mechanical stress. Such internal mechanical stresses cause no acceleration of that body as the forces balance one another. Pressure, the distribution of many small forces applied over an area of a body, is a simple type of stress that if unbalanced can cause the body to accelerate. Stress usually causes deformation of solid materials, or flow in fluids.</p>
    `,
    "output": `
Physics, force, motion, object, velocity, acceleration, push, pull, magnitude, direction, vector quantity, newton, symbol F, Newton's second law, momentum, net force, proportional, inverse, mass, thrust, drag, torque, rotational speed, extended body, internal mechanical stress, pressure, deformation, solid materials, flow, fluids.
    `
  },
];
