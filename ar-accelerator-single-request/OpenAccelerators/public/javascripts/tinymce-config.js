import {examples} from "./examples.js"
//left editor
tinymce.init({
    selector: '#editor',
    plugins: 'code image',
    toolbar: 'code aiToolsMenu aiToolsExamplesMenu',
    // toolbar:false,
    // menubar:false,
    height: '650px',
    statusbar:true,
    setup: function (editor) {
      // editor.ui.registry.addButton('aiAltTextButton', {
      //   text: 'Contextualize Alt Text',
      //   onAction: function (_) {
      //     sendPrompt('ContextualizeAltText');
      //   }
      // });
      // editor.ui.registry.addButton('accessibleTableButton', {
      //   text: 'Make Table Accessible',
      //   onAction: function (_) {
      //     sendPrompt('AccessibleTable');
      //   }
      // });
      editor.ui.registry.addMenuButton('aiToolsMenu', {
        text: 'AI Tools',
        fetch: function (callback) {
          
          var items = [
            {
              type: 'menuitem',
              text: 'Contextualize Alt Text',
              onAction: function () {
                sendPrompt('ContextualizeAltText');
                //editor.insertContent('&nbsp;<em>You clicked menu item 1!</em>');
              }
            },
            {
              type: 'menuitem',
              text: 'Make Table Accessible',
              onAction: function () {
                sendPrompt('AccessibleTable');
                //editor.insertContent('&nbsp;<em>You clicked menu item 1!</em>');
              }
            },
            {
              type: 'menuitem',
              text: 'Generate Keywords',
              onAction: function () {
                sendPrompt('GenerateKeywords');
              }
            }
          ];
          callback(items);
        }
      });
      editor.ui.registry.addMenuButton('aiToolsExamplesMenu', {
        text: 'AI Examples',
        fetch: function (callback) {
          var items = [
            {
              type: 'menuitem',
              text: examples[0].name,
              onAction: function () {
                editor.setContent(examples[0].input)
                setOutputEditor(examples[0].output);
              }
            },
            {
              type: 'menuitem',
              text: examples[1].name,
              onAction: function () {
                editor.setContent(examples[1].input)
                setOutputEditor(examples[1].output);
              }
            },
            {
              type: 'menuitem',
              text: examples[2].name,
              onAction: function () {
                console.log(editor.getContent({format:'text'}));
                editor.setContent(examples[2].input, { format:'text', content: examples[2].input});
                setOutputEditor(examples[2].output);
              }
            }
          ];
          callback(items);
        }
      });
    }
  });
//right editor
tinymce.init({
  selector: '#myeditable-div',
  plugins: 'code image',
  toolbar: 'code',
  // toolbar: false,
  menubar: false,
  statusbar: true,
  height: '650px'

});

//Loader 

function showLoader() {
  var loader = document.getElementById("loader");
  console.log("clicked");
  loader.style.display = 'block';

}

function hideLoader() {

  var loader = document.getElementById("loader");
  console.log("clicked");
  loader.style.display = 'none';
}

//adding a post api to send data to openai.

async function sendPrompt(toolname){
  showLoader()

  console.log('button pressed')

  var data = {passage: tinymce.get('editor').getContent(), tool: toolname};
// var data ={ tinymce.get('editor').getContent()};

// const htmlData = {data :'<h1>Hello World!</h1>'};
// console.log(data);
// tinymce.get('myeditable-div').setContent(data);
  console.log("stringified data " + JSON.stringify(data))
  console.log('<><><><><><><><><><><><><><><><><>');
  console.log("non stringified data " + data)

  await fetch("http://localhost:3000/frontendData", {
    method: "POST", // or 'PUT'
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),

  })
  .then(response => 
    // console.log(response);
    response.json())
  .then(data => 
    tinymce.get('myeditable-div').setContent(decodeURI((data["result"]))));
    hideLoader()
    //console.log("%%%%%%%%%%%%%%%%%%%%%%Success:"+ decodeURI((data["result"]))+"%%%%%%%%%%%%%%%%%%%%"))
//   .catch((error) =>{ 
//     console.error("Error:", error)
// });

// await fetch("http://localhost:3000/sendData",{

// method: "GET",
// headers:{
//   "Content-Type" : "application/json",
// },
// })
// // .then(response =>{
// //   response.json()
// // })
// .then(data => 
//   console.log("Success / Get Data Received : "  , data ));

  // fetch("http://localhost:3000/data", {
//   method: "POST", // or 'PUT'
//   headers: {
//     "Content-Type": "application/json",
//   },
//   body: JSON.stringify({
//     data : "jnsdkabkas"
// })  
    
// })
//   .then(response => 
//     // console.log(response);
//     response.json()
//   )
//   .then(data => 
//     console.log("Success:", data)
//   )
//   .catch((error) =>{ 
//     console.error("Error:", error)
// });


}

async function setOutputEditor(text, type='html'){
  tinymce.get('myeditable-div').setContent( text, { format: type });
}

async function showExample(toolname){
  //tinymce.get('myeditable-div').setContent(decodeURI((data["result"]))));
}

async function showSideBar(show){
  if(show){
    
  }else{

  }
}

// async function getResult (){

//   await fetch("http://localhost:3000/sendData",{

//   method: "GET",
//   headers:{
//     "Content-Type" : "application/json",
//   },
//   })
//   .then(response =>{
//     return response.json()
//   })
//   .then(data => 
//     //  console.log("Success / Get Data Received : "  , data ),
//      tinymce.get('myeditable-div').setContent(decodeURI((data["result"]))));
     
// }




// fetch('localhost:3000/', ()=>{

// })
// console.log('button pressed')
// const inputCellData = document.getElementById('editor').value;
//  document.getElementById('myeditable-div').innerHTML = inputCellData;
     


// outputCellData.value = inputCellData.value ;
// console.log(inputCellData.value)
// inputCellData.addEventListener("keyup", ()=>{
//   console.log(inputCellData.textContent)
// })
// outputCellData.innerText = "abasdasdasdadac";

// outputCellData.textContent = inputCellData.textContent;
// inputCellData.addEventListener('change', ()=>{


// })

