var express = require('express');
var path = require('path');
const { Configuration, OpenAIApi } = require("openai");
require("dotenv").config();
// var cookieParser = require('cookie-parser');
const bodyParser = require("body-parser")
var logger = require('morgan');
const { json } = require('express');
// const getAiData = require('./backend/controller/controller')
// const router = require('./backend/routes/routes')
var app = express();

const decodeJson = require("unescape-json");
const charUnescape = require('char-unescape')()



const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);


const allPromptTools = {
    'ContextualizeAltText':'Optimize and contextualize the alt and figcaption in the entire text below and replace them:\n\n',
    'AccessibleTable':'Convert table below to accessible table and write descriptions wherever applicable:\n\n',
    'GenerateKeywords':'Suggest Keywords for the paragraph below:\n\n'
}




// app.use('/', router);
// app.use(bodyParser.text())
app.use(bodyParser.json())
// app.use(bodyParser.raw())
app.use(logger('dev'));
// app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/* New Route to the TinyMCE Node module */
app.use('/tinymce', express.static(path.join(__dirname, 'node_modules', 'tinymce')));
var  data  ;
var  completion;  
var Result;
// app.post('/data', (req, res)=>{
// console.log('/DummyData');
//     const data = req.body
//     console.log(">>>>>><<<<<<")
//     console.log(data)
//     res.send('dummy data received successfully')
// })

// app.get('/test', (req: Request, resp: Response): void => {
//     resp.status(200).json({ data: "test page123" })
// })


app.post('/frontendData', async (req, res) => {
    console.log('ActualData');
    data = await req.body
    passage = JSON.stringify(data["passage"])
    console.log(">>>>>>api result<<<<<<")
    console.log(passage)
    let tool = data["tool"];
    pppp = "" + allPromptTools[tool] + passage;
    console.log(pppp)

    completion = await openai.createCompletion({
        model: "text-davinci-003",
        prompt: allPromptTools[tool] + passage,
        // prompt:JSON.stringify(data["passage"]),
        temperature: 0,
        max_tokens: 600
    });
    Result = { result: completion.data.choices[0].text }
    console.log(Result["result"].replace(/\\\\/g, '\\'));
    Result["result"] = encodeURI(Result["result"].replace(/\\\\/g, '\\'));
    const decodedJson = JSON.stringify(Result)
    console.log("******** " + (decodedJson) + " ***********");
    res.status(200).send(decodedJson);

    // console.log('consoling openai result >>>>>>>>    ' + completion.data.choices[0].text + '   <<<<<<<<<<< end');
    // res.send('Actual data received successfully'+ completion.data.choices[0].text)

})
// const query = " whats the fullform of  " + data;
 
// const abc = async ()=>{

//     completion = await openai.createCompletion({
//        model: "text-davinci-003",
//        prompt:query,
//        temperature: 0.6,
//      });
//      return completion;
// }

// async function printResult(){

//     console.log(completion.data.choices[0].text);
// }


// app.get('/sendData', async (req, res)=>{
//     // var decodedJson  = decodeJson(JSON.stringify(Result));
//     Result["result"] = encodeURI(charUnescape(Result["result"]));
//  const decodedJson = JSON.stringify(Result)
//     console.log("******** "+(decodedJson)+" ***********");
//     res.status(200).send(decodedJson);

// })


app.listen(3000,()=>{
console.log('server listening on port 3000');
})

// module.exports = app;